package com.m46.codechecks.controller;

import com.m46.codechecks.model.EmailVerification;
import com.m46.codechecks.model.VerificationRequest;
import com.m46.codechecks.service.EmailVerificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/email")
public class EmailVerificationController {
    private final EmailVerificationService verificationService;
//
//    @GetMapping(path = "/{email}")
//    @ResponseStatus(HttpStatus.OK)
//    @ApiOperation(
//            value = "",
//            notes = "Utility method to check current state of the email verification process.")
//    public EmailVerificationState getVerificationState(
//            @PathVariable String email
//    ){
//
//        return verificationService.getVerificationState(email);
//    }

    @PostMapping(path = "/send")
    @ResponseStatus(HttpStatus.CREATED)
    public void requestEmailVerification(@RequestBody
                    VerificationRequest verificationRequest
    ) {
        log.info("New verification for {} {}", verificationRequest.getFirstName(), verificationRequest.getEmail());
        verificationService.requestEmailVerification(verificationRequest.getEmail(),
                verificationRequest.getFirstName());
    }

    @PostMapping(path = "/check")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity verify(@RequestBody EmailVerification verification
    ) {
        log.info("Verify {} with code '{}'", verification.getEmail(), verification.getVerificationCode());
        boolean bol = verificationService.verify(verification.getEmail(), verification.getVerificationCode());
        if (bol){

            RestTemplate restTemplate = new RestTemplate();

            String url = "http://164.90.230.139:8081/api/captcha/question";
            return ResponseEntity.ok(restTemplate.getForObject(url,String.class));

        }
        return ResponseEntity.ok("error");
    }

}
