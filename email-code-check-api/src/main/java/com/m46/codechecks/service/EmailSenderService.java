package com.m46.codechecks.service;

import com.m46.codechecks.config.EmailCodeCheckProps;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailSenderService {

    private final EmailCodeCheckProps verificationProps;
    private final JavaMailSender javaMailSender;



    public void sendVerificationEmail(String email, String customerName, String code) {
        sendEmail(email, customerName, code, verificationProps.getEmailTemplateId());
    }

    public void sendEmail(String email, String customerName, String code, Long templateId) {
        SimpleMailMessage massage = new SimpleMailMessage();
        massage.setFrom("ahmedd000001@gmail.com");
        massage.setTo(email);
        massage.setSubject("Жесткий тип приветствует вас "+customerName);
        massage.setText("Ваш код: "+code);
        javaMailSender.send(massage);




    }

}
