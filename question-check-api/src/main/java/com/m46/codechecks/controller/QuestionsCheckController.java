package com.m46.codechecks.controller;

import com.m46.codechecks.model.dto.Question;
import com.m46.codechecks.model.dto.QuestionCaptchaDto;
import com.m46.codechecks.model.dto.QuestionCheckRequest;
import com.m46.codechecks.service.QuestionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/captcha")
public class QuestionsCheckController {

    private final QuestionService questionService;

    @GetMapping(path = "/question")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Question> get() {

        return ResponseEntity.ok(questionService.getAllQuestion());

    }

    @PostMapping(path = "/answer")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> check(@RequestBody QuestionCheckRequest questionCheckRequest) {
        System.out.println("Hi how are you");

        return ResponseEntity.ok(questionService.getAnswerResult(questionCheckRequest));

    }
}
