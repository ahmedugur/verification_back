package com.m46.codechecks.service;

import com.m46.codechecks.model.dto.Question;
import com.m46.codechecks.model.dto.QuestionCaptchaDto;
import com.m46.codechecks.model.dto.QuestionCheckRequest;
import com.m46.codechecks.model.dto.QuestionDto;
import com.m46.codechecks.model.entity.QuestionEntity;
import com.m46.codechecks.repository.QuestionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class QuestionService {

    private final QuestionRepository questionRepository;

    private final ModelMapper modelMapper;

    public Question getAllQuestion() {
        List<QuestionEntity> list = questionRepository.findAll();
        QuestionEntity entity = list.get((int) (1+Math.random()*list.size()-1));

        QuestionDto questionDto = modelMapper.map(entity, QuestionDto.class);
        Question question = new Question();
        question.setQuestion(questionDto.getQuestion());
        question.setQuestionId(questionDto.getQuestionId());
        return question;
    }

    public Object getAnswerResult(QuestionCheckRequest questionCheckRequest) {
        Optional<QuestionEntity> questionEntity = questionRepository.findById(questionCheckRequest.getQuestionId());

        if(Objects.equals(questionCheckRequest.getAnswer(),questionEntity.get().getAnswer())){

            return questionCheckRequest;

        }
        return new Object();
    }
}
