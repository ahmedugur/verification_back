package com.m46.codechecks.model.dto;


import lombok.Data;

@Data
public class QuestionCaptchaDto {

    private int questionId;
    private String answer;
}
