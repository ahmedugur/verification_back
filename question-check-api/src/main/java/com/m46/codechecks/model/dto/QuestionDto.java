package com.m46.codechecks.model.dto;

import lombok.Data;

@Data
public class QuestionDto {

    private String question;
    private String answer;
    private Integer questionId;
}
