package com.m46.codechecks.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PersonDto {
        private Integer id;
        private String email;
        private String name;
        private String surname;
        private String patronymic;
        private ProductDto productDto;

}
