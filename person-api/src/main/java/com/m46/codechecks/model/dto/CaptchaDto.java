package com.m46.codechecks.model.dto;


import lombok.Data;

@Data
public class CaptchaDto {
    private String questionId;
    private String answer;
    private Integer personId;
}
