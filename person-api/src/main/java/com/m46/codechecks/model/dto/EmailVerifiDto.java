package com.m46.codechecks.model.dto;

import lombok.Data;

@Data
public class EmailVerifiDto {

    private String firstName;
    private String email;
}
