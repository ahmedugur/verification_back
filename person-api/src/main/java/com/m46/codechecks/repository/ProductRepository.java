package com.m46.codechecks.repository;

import com.m46.codechecks.model.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity,Integer> {



    @Modifying
    @Query("update ProductEntity p " +
            "set p.status = :newStatus " +
            "where p.personId = :personId")
    void updateVerificationStatus(
            @Param("personId")Integer personId,
            @Param("newStatus")String newStatus
    );

}
