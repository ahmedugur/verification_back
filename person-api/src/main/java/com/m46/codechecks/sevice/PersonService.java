package com.m46.codechecks.sevice;

import com.m46.codechecks.model.dto.CaptchaDto;
import com.m46.codechecks.model.entity.PersonEntity;
import com.m46.codechecks.model.dto.PersonDto;
import com.m46.codechecks.model.dto.ProductDto;
import com.m46.codechecks.model.entity.ProductEntity;
import com.m46.codechecks.repository.PersonRepository;
import com.m46.codechecks.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class PersonService {

    private final PersonRepository personRepository;
    private final ModelMapper modelMapper;
    private final ProductService productService;
    private final ProductRepository productRepository;

    public Object create(PersonDto personDto) {
        System.out.println(personDto.toString());
        PersonEntity personEntity = modelMapper.map(personDto, PersonEntity.class);
        personEntity = personRepository.save(personEntity);

        if (Objects.nonNull(personDto.getProductDto())) {
            ProductDto productDto = personDto.getProductDto();
            productDto.setPersonId(personEntity.getId());

            ProductEntity productEntity = modelMapper.map(productDto, ProductEntity.class);
        } else {
            throw new RuntimeException("PRODUCT OT FOUND");
        }
        return personEntity;

    }

    public void updateProductStatus(CaptchaDto captchaDto){
        Integer personId = captchaDto.getPersonId();
        System.out.println("SERVICE "+captchaDto.toString());
        productRepository.updateVerificationStatus(personId,"Done");
    }
}
