package com.m46.codechecks.sevice;

import com.m46.codechecks.model.entity.ProductEntity;
import com.m46.codechecks.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@RequiredArgsConstructor
public class ProductService {


    private final ProductRepository productRepository;

    public Object create(ProductEntity productEntity){
        productRepository.save(productEntity);

        return new Object();
    }
}
