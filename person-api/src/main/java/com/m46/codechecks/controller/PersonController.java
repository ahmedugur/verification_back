package com.m46.codechecks.controller;

import com.m46.codechecks.model.dto.CaptchaDto;
import com.m46.codechecks.model.dto.EmailVerifiDto;
import com.m46.codechecks.model.dto.PersonDto;
import com.m46.codechecks.model.dto.Question;
import com.m46.codechecks.sevice.PersonService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/person")
public class PersonController {
    private final PersonService personService;

    @GetMapping(path = "")
    @ResponseStatus(HttpStatus.CREATED)
    public Question get() {

        return null;
    }

//    @PostMapping(path = "")
//    @ResponseStatus(HttpStatus.OK)
//    @ApiOperation(
//            value = "",
//            notes = "")
//    public QuestionCheckStatus check(@RequestBody QuestionCheckRequest questionCheckRequest) {
//
//        return null;
//    }

    @PostMapping(path = "/add")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> check(@RequestBody PersonDto personDto) {
        System.out.println(personDto.toString());

        RestTemplate restTemplate = new RestTemplate();

        EmailVerifiDto dto = new EmailVerifiDto();

        dto.setFirstName(personDto.getName());
        dto.setEmail(personDto.getEmail());
        String url = "http://164.90.230.139:8082/api/email/send";

        System.out.println("DTO "+dto.getFirstName()+" "+dto.getEmail());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<EmailVerifiDto> emailVerifiDtoHttpEntity = new HttpEntity<>(dto,headers);
        restTemplate.postForObject(url, emailVerifiDtoHttpEntity, String.class);
        return ResponseEntity.ok(personService.create(personDto));
    }

    @PostMapping(path = "/update")
    @ResponseStatus(HttpStatus.OK)
    public void captchaCheck(@RequestBody CaptchaDto captchaDto) {
        System.out.println("DYO "+captchaDto.toString());
       personService.updateProductStatus(captchaDto);

    }
}
